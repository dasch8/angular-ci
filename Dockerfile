FROM node:22-alpine

# Install Chromium
RUN apk add --update --no-cache\
    chromium-chromedriver\
    chromium

# Install Angular CLI
RUN npm install --global @angular/cli@19 && \
    npm cache clean --force

CMD ["sh"]
