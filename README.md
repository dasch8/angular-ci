# Docker Container for Angular CI
This is a docker container to run continuous integration tasks for Angular projects using the CLI.

*Disclaimer: I'm using this image for some projects of mine and update it depending on the needs of those.
Feel free to fork or download the Dockerfile and tweak it to your own needs though.*

## Project Configuration
To use the container for your project you need to configure Karma and Protractor to use headless Chromium.

### Karma
Add the following to `karma.conf.js:`
```javascript
customLaunchers: {
  ChromiumCI: {
    base: 'ChromiumHeadless',
    flags: ['--no-sandbox', '--disable-dev-shm-usage']
  }
}
```
You can then run tests in your CI server using the `--browsers` option of `ng test`:
```
ng test --browsers=ChromiumCI
``` 

### Protractor
Create a separator configuraton file for protractor next to the default one e.g. `protractor-ci.conf.js` with the following content:
```javascript
const config = require('./protractor.conf').config;

config.chromeDriver = '/usr/bin/chromedriver';
config.capabilities = {
    browserName: 'chrome',
    chromeOptions: {
        args: [
            '--headless',
            '--no-sandbox',
            '--disable-gpu',
            '--disable-dev-shm-usage'
        ]
    }
};

exports.config = config;
```
Then in `angular.json` add this configuration to the e2e project.
```json
"ci": {
  "protractorConfig": "e2e/protractor-ci.conf.js"
}
```
You can then run e2e tests in your CI server using the `--configuration` option of `ng e2e`:
```
ng e2e --configuration ci
```
